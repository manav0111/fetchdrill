// Using the Fetch API to make a GET request to the specified URL
fetch("https://jsonplaceholder.typicode.com/todos")
    .then((data) => {
        // Parsing the response body as JSON
        const res = data.json();
        return res; // Returning the parsed JSON data
    })
    .then((res) => {
        // Logging the parsed JSON data to the console
        console.log(res);
    })
    .catch((err) => {
        // Handling errors if the request fails
        console.log(err);
    });
