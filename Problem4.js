// Fetching user data from the specified URL
fetch("https://jsonplaceholder.typicode.com/users")
    .then((data) => {
        // Parsing the response body as JSON for user data
        const res = data.json();
        return res; // Returning the parsed user data
    })
    .then((res) => {
        // Iterating through each user in the user data
        res.forEach((user) => {
            // Extracting user ID
            const userId = user.id;

            // Fetching todos based on user ID
            const todosBasedOnID = fetch(`https://jsonplaceholder.typicode.com/todos?userId=${userId}`);

            // Processing todos based on user ID
            todosBasedOnID
                .then((res) => {
                    return res.json(); // Parsing the response body as JSON for todos data
                })
                .then((todosData) => {
                    // Logging todos data to the console
                    console.log(todosData);

                    // Iterating through each todo in the todos data
                    todosData.forEach((data) => {
                        // Logging details of each todo to the console
                        console.log(`Userid:${data.userId}, Id:${data.id}, Title:${data.title}, and Completed: ${data.completed}`);
                    });
                })
                .catch((err) => console.log(err)); // Handling errors if fetching todos fails
        });
    })
    .catch((err) => console.log(err)); // Handling errors if fetching users fails
