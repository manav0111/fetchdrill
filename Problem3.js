// Using the Fetch API to get user data from the specified URL
fetch("https://jsonplaceholder.typicode.com/users")
    .then((data) => {
        // Parsing the response body as JSON for user data
        const res = data.json();
        return res; // Returning the parsed user data
    })
    .then((res) => {
        // Logging user data to the console
        console.log("User Data");
        console.log(res);
    })
    .then(() => {
        // Fetching todo data after fetching user data using a nested promise chain
        fetch("https://jsonplaceholder.typicode.com/todos")
            .then((data) => {
                // Parsing the response body as JSON for todo data
                const res = data.json();
                return res; // Returning the parsed todo data
            })
            .then((res) => {
                // Logging todo data to the console
                console.log("Todo Data");
                console.log(res);
            });
    })
    .catch((err) => {
        // Handling errors if any of the requests fail
        console.log(err);
    });
