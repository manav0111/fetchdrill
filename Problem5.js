fetch("https://jsonplaceholder.typicode.com/todos")
  .then((data) => {
    const res = data.json();
    return res;
  })
  .then((res) => {
    // console.log(res);
    const User=res[0];

    const UserInfo=fetch(`https://jsonplaceholder.typicode.com/users?id=${User.userId}`);

    UserInfo
    .then((data)=>{ 
        return data.json();
    })
    .then((res)=>{
        console.log(res);
    })

    // console.log(User);
  })
  .catch((err) => {
    // Handling errors if the request fails
    console.log(err);
  });
